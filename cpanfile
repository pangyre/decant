requires "Mojolicious" => "4.88";
requires "EV" => "4.0";
requires "IO::Socket::IP" => "0.20";
requires "IO::Socket::SSL" => "1.75";
requires "Mojolicious::Plugin::OAuth2" => "1.0";
